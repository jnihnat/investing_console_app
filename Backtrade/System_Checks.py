import os

home = os.path.expanduser("~")
BT_home = str(home + '/Documents/Backtrade/')


def path_check():
    if not os.path.exists(BT_home):
        os.makedirs(BT_home)

    if not os.path.exists(BT_home + '/Data/StockData'):
        os.makedirs(BT_home + '/Data/StockData')

    if not os.path.exists(BT_home + '/Data/IndexData'):
        os.makedirs(BT_home + '/Data/IndexData')

    if not os.path.exists(BT_home + '/Calculated_Tables'):
        os.makedirs(BT_home + '/Calculated_Tables')

    if not os.path.exists(BT_home + '/Logs'):
        os.makedirs(BT_home + '/Logs')
